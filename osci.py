import numpy as np
import numpy.linalg as npla
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import matlibplots.conv_plot_utils as cpu

import logging
import logging.config

import yaml
logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger('basic')
logger.setLevel('INFO')  # default is DEBUG


# parameters
k1, k2 = 1., 1.
b1, b2 = -1., .4
rhofrac = 0.55
wnrm = 2  # 'fro'
omega = - 0.5 * (b1 + b2)


def sysmat(avec):
    # absao = 0*avec[0]**2
    absao = avec[0]**2
    # absao = np.abs(avec[0])
    return np.array([[b1, k1*(1.+absao)], [-k2*(1.+absao), b2]])


def evals(xvec):
    camat = sysmat(xvec)
    deta = npla.det(camat)
    traa = camat.trace()
    imgpart = np.sqrt(.25*traa**2 - deta + 0j)
    lone = traa/2 + imgpart
    ltwo = traa/2 - imgpart
    return lone, ltwo


def evecs(xvec, get_cond=True):
    camat = sysmat(xvec)
    lone, ltwo = evals(xvec)
    evone = np.array([[lone-camat[1, 1], camat[1, 0]]]).T
    evtwo = np.array([[ltwo-camat[1, 1], camat[1, 0]]]).T
    if get_cond:
        evmat = np.c_[evone, evtwo]
        cond = npla.norm(evmat, ord=wnrm) * \
            npla.norm(np.linalg.inv(evmat), ord=wnrm)
        return evone, evtwo, cond
    return evone, evtwo


def ffunc(avec, t):
    csa = sysmat(avec)
    return np.dot(csa, avec)


def trajstats(sol, tmesh=None, smplrt=5):
    evtfilter = np.arange(0, Nts, smplrt)
    evlist = []
    nrmlist = []
    ftmesh = []
    klist = []
    mtlist = []
    csollist = []
    for ft in evtfilter:
        csol = sol[ft, :]
        csaevs = np.array(evals(csol))
        _, _, csak = evecs(csol)
        evlist.append(csaevs)
        nrmlist.append(npla.norm(csol))
        ftmesh.append(tmesh[ft])
        klist.append(csak)
        csollist.append(csol)
        mtlist.append(compmt(csollist, tmesh=ftmesh, omega=omega))

    return ftmesh, evlist, nrmlist, klist, mtlist


def enudenovals(omega, t=None, s=None, rho=None, xs=None,
                xrho=None, Arho=None):
    ''' enumerator and denominator functions for the comp of `mt`

    '''
    logger.debug('got: omega={0}, t={1}, s={2}, rho={3}, xs={4}, xrho={5}'.
                 format(omega, t, s, rho, xs, xrho))
    if Arho is None:
        Arho = sysmat(xrho)
    enuval = np.exp(-omega*(t-s))*npla.norm(sysmat(xs)-Arho, ord=wnrm) * \
        npla.norm(xs)
    denoval = np.exp(-omega*(t-s))*np.abs(s-rho)*npla.norm(xs)

    return enuval, denoval


def compmt(sol, tmesh=None, rhoidx=None, omega=None):
    # we use pw. trap rule to compute the integrals
    if len(tmesh) == 1:
        return 0
    if rhoidx is None:
        # rhoidx = 6*len(tmesh)/13
        rhoidx = np.rint(rhofrac*len(tmesh)).astype(int)
    rho = tmesh[rhoidx]
    xrho = sol[rhoidx]
    Arho = sysmat(xrho)
    endt = tmesh[-1]

    inixs, inis = sol[0], tmesh[0]
    cenuval, cdenoval = enudenovals(omega, t=endt, s=inis, rho=rho,
                                    xs=inixs, xrho=xrho, Arho=Arho)
    enuint, denoint = 0, 0
    for k in range(1, len(tmesh)):
        dt = tmesh[k] - tmesh[k-1]
        curxs, curs = sol[k], tmesh[k]
        nenuval, ndenoval = enudenovals(omega, t=endt, s=curs, rho=rho,
                                        xs=curxs, xrho=xrho, Arho=Arho)
        enuint += dt*0.5*(nenuval+cenuval)
        denoint += dt*0.5*(ndenoval+cdenoval)
        cenuval, cdenoval = nenuval, ndenoval
    return enuint/denoint


def plotit(ftmesh=None, evlist=None, nrmlist=None, klist=None, mtlist=None):

        if evlist is not None:
            levs = np.array(evlist)
            plt.figure(2)
            plt.plot(np.real(levs), np.imag(levs), '+')

        if nrmlist is not None:
            plt.figure(3)
            plt.plot(ftmesh, nrmlist)

        if klist is not None:
            plt.figure(4)
            plt.plot(ftmesh, klist)

        if mtlist is not None:
            plt.figure(5)
            plt.plot(ftmesh, mtlist)

        plt.show(block=False)


def compomegs(omega=None, K=None, mt=None, tstar=None, xzrad=None,
              itsanarray=None):
    logk = np.log(np.max([np.max(K), 2]))
    print logk
    omt = - (np.sqrt(mt*K*logk) - omega)
    oms = -(logk/tstar - omt)
    if itsanarray:
        return oms, omt
    else:
        print 'omtstar = {0:.4f}, omstar = {1:.4f}'.format(omt, oms)
        print 'rho={0:.4f}, omega={1}, rad={2}, tstar={3}, K={4}, mt={5}'.\
            format(rhofrac, omega, xzrad, tstar, K, mt)


if __name__ == '__main__':
    xzrad = .25
    # xzrad = 2.
    nxbsmpls = 12  # number of points on the boundary that are evaluated
    numcircs = 3  # number of inner circles on which we check
    kll, mtll, nrmll = [], [], []
    quickplot = False  # whether to plot it here
    np.random.seed(0)  # for replicability

    for N in range(numcircs):
        cnfac = (numcircs - N)*1./numcircs
        xzradn = cnfac*xzrad
        cnxbsmpls = max(1, np.floor(cnfac*nxbsmpls))
        logger.info('checking {0} points on the radius {1}'.
                    format(cnxbsmpls, xzradn))
        rndn = np.random.rand()
        xbsmpls = np.linspace(0+rndn, 2*np.pi+rndn, cnxbsmpls+1)
        for theta in xbsmpls.tolist()[1:]:
            inia = xzradn*np.array([[np.cos(theta), np.sin(theta)]]).T

            t = 0
            tE, Nts = 20., 400
            samplerate = 5
            print 'eigenvalues at t={0}: '.\
                format(t), np.linalg.eigvals(sysmat(inia))

            tmesh = np.linspace(0, tE, Nts+1)

            sol = odeint(ffunc, inia.flatten(), tmesh)

            ftmesh, evlist, nrmlist, klist, mtlist =\
                trajstats(sol, tmesh=tmesh, smplrt=samplerate)
            kll.append(klist)
            mtll.append(mtlist)
            nrmll.append(nrmlist)
            if quickplot:
                plotit(ftmesh=ftmesh, evlist=evlist, nrmlist=nrmlist,
                       klist=klist, mtlist=mtlist)

    mtla = np.array(mtll)
    mtlmx = np.max(mtla, axis=0)

    kla = np.array(kll)
    klamx = np.max(kla, axis=0)
    for k in range(klamx.size)[1:]:
        klamx[k] = np.max([klamx[k], klamx[k-1]])

    ftma = np.array(ftmesh).reshape((1, len(ftmesh)))
    mtlmx = mtlmx.reshape((1, len(ftmesh)))
    klamx = klamx.reshape((1, len(ftmesh)))

    oms, omt = compomegs(omega=omega, K=klamx, mt=mtlmx, tstar=ftma,
                         itsanarray=True)
    omt[0][0] = 0

    parastr = '-alpha{0}-rho{1}-xzrad{2}'.format(b2, rhofrac, xzrad)
    cpu.para_plot(ftmesh, nrmll, xlabel='t', ylabel='$\\|\\xi(t)\\|$',
                  title=None, fignum=111,
                  tikzfile='trjnrms' + parastr + '.tikz')

    cpu.para_plot(ftmesh, kll, xlabel='t', ylabel='$K(t)$', title=None,
                  fignum=112,
                  tikzfile='ks' + parastr + '.tikz')

    cpu.para_plot(ftmesh, mtll, xlabel='t', ylabel='$m_t$',
                  title=None, fignum=113,
                  tikzfile='mtlls' + parastr + '.tikz')

    cpu.para_plot(ftmesh, [-oms.flatten(), [-omega]*ftma.size, [0]*ftma.size],
                  xlabel='$t^*$',
                  leglist=['$-\\omega^*(t^*)$', '$-\omega$', 'zero'],
                  title=None, fignum=114,
                  tikzfile='oms' + parastr + '.tikz')

    print 'rhofrac={0}, omega={1}, xzrad={2}'.format(rhofrac, omega, xzrad)
    print 'try \n compomegs(omega={0}, K=k, mt=mt, tstar=ts, xzrad={1})'.\
        format(omega, xzrad)
