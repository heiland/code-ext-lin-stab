### Numerical Tests of the preprint

The example that illustrates the bounds is defined in `osci.py`

The example on the stabilization of the *5D example* by sylvester updates is started through `run_5dBanLT.py`.

The example on the stabilization of the *Chaffee Infante example* by sylvester updates is started through `run_schloegl.py`.

In both cases there is an option `fbstats=True` that will return the plots of the decay rates. Set it `False` for the timings.

To measure the computation times, start *IPython* and launch `run time_ricstabcalls.py` to setup the test matrices. And then use `%timeit ...` as indicated by the output of the script.

The timings reported in the preprint were obtained on a PC with the hardware/software specifications as listed in `computerinfo.out`.

### Dependencies -- versions used in the numerical tests
 * numpy -- version 1.8.2
 * scipy -- 0.17.0.dev0+324ac7a
 * dolfin (fenics) -- 1.5 (also works with dolfin 2016.2.0+)
 * [matlibplots](https://github.com/highlando/mat-lib-plots)  -- for plotting
 * [dolfin_navier_scipy](https://github.com/highlando/sadptprj_riclyap_adi) -- for interfacing between *FEniCS* and *Scipy*
 * my own python modules with helper functions for the output and timings that can be obtained from my github account or simply via the shell command `source get_upd_pymodules.sh ` 

#### Partially 
Alternatively, you can switch to the branch `all-gh-deps-incl` that comes with all my github sources and the script `get_upd_pymodules.sh` for updating.

### Cite as
```
@Misc{Hei16_ext-lin-stab,
  Title                    = {ext-lin-stab -- {P}ython Module for stabilization of SDC systems by linear updates of Riccati feedback},
  Author                   = {Jan Heiland},
  HowPublished             = {\url{https://gitlab.mpi-magdeburg.mpg.de/heiland/code-ext-lin-stab}},
  Year                     = {2016},
  Url                      = {https://gitlab.mpi-magdeburg.mpg.de/heiland/code-ext-lin-stab}
}
```
