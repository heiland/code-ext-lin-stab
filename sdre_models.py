import numpy as np
import numpy.linalg as npla


class OsciModel():
    ''' the model of the tweaked oscillator with the SDC

    A = b1              k1*(1+x1**2)
        k1*(1+x1**2)    b2

    with default

    A = -1         1+x1**2
        1+x1**2    .5

    coming with methods for eigenvalues, eigenvectors and condition of
     the matrix of eigenvectors, and `A(x)x`
    '''

    def __init__(self, b2=.5, wnrm=2):
        # parameters
        self.k1, self.k2 = 1., 1.
        self.b1, self.b2 = -1., b2
        self.omega = 0.5 * (-self.b1 - self.b2)
        self.wnrm = wnrm
        self.B = np.array([0., 1.]).reshape((2, 1))
        self.C = np.array([0., 1.]).reshape((1, 2))
        self.sdim = 2

    def sysmat(self, avec):
        absao = avec[0]**2
        return np.array([[self.b1, self.k1*(1.+2.*absao)],
                         [-self.k2*(1.+1.*absao), self.b2]])

    def evals(self, xvec):
        camat = self.sysmat(xvec)
        deta = npla.det(camat)
        traa = camat.trace()
        imgpart = np.sqrt(.25*traa**2 - deta + 0j)
        lone = traa/2 + imgpart
        ltwo = traa/2 - imgpart
        return lone, ltwo

    def evecs(self, xvec, get_cond=False):
        camat = self.sysmat(xvec)
        lone, ltwo = self.evals(xvec)
        evone = np.array([[lone-camat[1, 1], camat[1, 0]]]).T
        evtwo = np.array([[ltwo-camat[1, 1], camat[1, 0]]]).T
        if get_cond:
            evmat = np.c_[evone, evtwo]
            cond = npla.norm(evmat, ord=self.wnrm) * \
                npla.norm(np.linalg.inv(evmat), ord=self.wnrm)
            return (evone, evtwo), cond
        return (evone, evtwo)

    def ffunc(self, avec, t=None):
        csa = self.sysmat(avec)
        return np.dot(csa, avec)


class BanLT5DModel():
    ''' the `5D` model from Chapter 3.4 of *Banks, Lewis, Tran 2007*

    coming with methods for eigenvalues, eigenvectors and condition of
     the matrix of eigenvectors, and `A(x)x`
    '''

    def __init__(self, wnrm=2, **kwargs):
        # parameters
        self.B = np.array([[0., 0.], [0., 0.], [1., 0.], [0., 0.], [0., 1.]])
        # self.C = np.array([[1., 0., 0., 0., 0.],
        #                    [0., 1., 0., 0., 0],
        #                    [0., 0., 0., 1., 0]])  # not observable !!!
        # self.C = np.eye(5)
        self.C = np.array([[1., 0., 0., 0., 0.],
                           [0., 0., 0., 1., 0]])
        # self.C = self.B.T  # not observable !!!
        self.sdim = 5
        self.wnrm = wnrm
        self.definival = np.array([.4, -.2, .1, -.1, .5]).reshape((5, 1))
        # an inival for which an initial feedback fails
        self.badinival = np.array([[-1.3], [-1.4], [-1.1], [-2.], [0.3]])

    def getinival(self, inivtype='defini', **kwargs):
        if inivtype == 'defini':
            return self.definival
        if inivtype == 'badini':
            return self.badinival

    def sysmat(self, avec):
        return np.array([[0., 1., 0., 0., 0.],
                         [0., 0., 1., 0., 0.],
                         [0., 0., 0., avec[3]**2, 0.],
                         [-avec[0], 0., 0., avec[3]**2, 1.],
                         [0., 0., 0., 0., 0.]])

    def evals(self, xvec):
        camat = self.sysmat(xvec)
        return npla.eigvals(camat)

    def evecs(self, xvec, get_cond=False):
        camat = self.sysmat(xvec)
        _, eigvecs = npla.eig(camat)
        if get_cond:
            cond = npla.norm(eigvecs, ord=self.wnrm) * \
                npla.norm(np.linalg.inv(eigvecs), ord=self.wnrm)
            return eigvecs, cond
        return eigvecs

    def ffunc(self, avec, t=None):
        csa = self.sysmat(avec)
        return np.dot(csa, avec)


class Schloegl1DNeumanContrl():
    ''' The Schloegl model as described in the PhD thesis by Altmueller

    A PDE model that can be scaled in dimension
    '''

    def __init__(self, N=None, xmin=0.0, xmax=1.0, ny=5, nonlpara=None,
                 **kwargs):
        import schloegl_model as schlglm
        M, A, B, rhsa, getschloeglsdc, schloegl_nonl =\
            schlglm.schloegl_spacedisc(N=N, a=xmin, b=xmax, nonlpara=nonlpara,
                                       retgetsdc=True)
        mdns, adns = np.array(M.todense()), np.array(A.todense())
        self.minv = npla.inv(mdns)
        self.minva = np.dot(self.minv, adns)
        self.B = np.dot(self.minv, B)
        czero = np.zeros((ny, N))
        for k in range(ny):
            czero[k, k*np.floor(N/ny)] = 1
        self.C = czero
        self.definival = 0.2*np.sin(np.linspace(xmin, 1., N+1)*np.pi)[:-1]
        self.schlglsdc = getschloeglsdc
        self.xmin, self.xmax, self.N = xmin, xmax, N

    def getinival(self, inivtype='defini', **kwargs):
        return self.definival

    def sysmat(self, avec):
        schlglsdcpart = self.schlglsdc(avec)
        return self.minv*schlglsdcpart - self.minva

    def evals(self, xvec):
        camat = self.sysmat(xvec)
        return npla.eigvals(camat)

    def evecs(self, xvec, get_cond=False):
        camat = self.sysmat(xvec)
        _, eigvecs = npla.eig(camat)
        if get_cond:
            cond = npla.norm(eigvecs, ord=self.wnrm) * \
                npla.norm(np.linalg.inv(eigvecs), ord=self.wnrm)
            return eigvecs, cond
        return eigvecs

    def ffunc(self, avec, t=None):
        csa = self.sysmat(avec)
        return np.dot(csa, avec)

    def solsurfplot(self, sol, addzerocol=True, tmesh=None, fignum=312,
                    datfile=None):
        from matplotlib import cm
        from mpl_toolkits.mplot3d import Axes3D
        # from matplotlib.ticker import LinearLocator, FormatStrFormatter
        import matplotlib.pyplot as plt
        import numpy as np

        fig = plt.figure(fignum)
        ax = Axes3D(fig)  # fig.gca(projection='3d')
        X = np.linspace(self.xmin, self.xmax, self.N+1)
        X, Y = np.meshgrid(X, tmesh)

        if addzerocol:
            sol = np.c_[sol, np.zeros((tmesh.size, 1))]
        surf = ax.plot_surface(X, Y, sol, rstride=1, cstride=1,
                               cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.show(block=False)
        # xvec = X.T.reshape((X.size, 1)).flatten()
        # yvec = Y.T.reshape((X.size, 1)).flatten()
        # solvec = sol.T.reshape((X.size, 1)).flatten()

        if datfile is not None:
            datafile = open(datfile, 'w')
            for j in range(X.shape[1]):
                for k in range(X.shape[0]):
                    # datafile.write('{0} {1} {2}\n'.
                    #                format(xvec[k], yvec[k], solvec[k]))
                    datafile.write('{0} {1} {2}\n'.
                                   format(X[k, j], Y[k, j], sol[k, j]))
                datafile.write('\n')

            datafile.close()
