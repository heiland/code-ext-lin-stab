import numpy as np
import numpy.linalg as npla


def twobtwo_evals(mat):
    deta = npla.det(mat)
    traa = mat.trace()
    imgpart = np.sqrt(.25*traa**2 - deta + 0j)
    lone = traa/2 + imgpart
    ltwo = traa/2 - imgpart
    return (lone, ltwo)


def get_evals(mat):
    if mat.shape[0] == 2:
        return twobtwo_evals(mat)
    else:
        return npla.eigvals(mat)


def twobtwo_evecs(mat, get_evec_cond=False, wnrm=2):
    lone, ltwo = twobtwo_evals(mat)
    evone = np.array([[lone-mat[1, 1], mat[1, 0]]]).T
    evtwo = np.array([[ltwo-mat[1, 1], mat[1, 0]]]).T
    if get_evec_cond:
        evmat = np.c_[evone, evtwo]
        cond = npla.norm(evmat, ord=wnrm) * \
            npla.norm(np.linalg.inv(evmat), ord=wnrm)
        return (evone, evtwo), cond
    return (evone, evtwo)


def get_evecs(mat, get_evec_cond=False, ret_evals=False, wnrm=2):
    eigvals, eigvecs = npla.eig(mat)
    reteigs = (eigvals, eigvecs) if ret_evals else eigvecs
    if get_evec_cond:
        cond = npla.norm(eigvecs, ord=wnrm) * \
            npla.norm(np.linalg.inv(eigvecs), ord=wnrm)
        return reteigs, cond
    return reteigs
