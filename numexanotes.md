Examples in `osci.py`
---
When omegastar is greater 0:
#### yes
`omtstar = 0.0885, omstar = 0.0297, rhofrac=0.3077, omega=0.4, xzrad=1, tstar=11.8, K=2.0, mt=0.07`
`omtstar = 0.0670, omstar = 0.0045, rhofrac=0.3300, omega=0.4, xzrad=1, tstar=11.1, K=2.0, mt=0.08`
`omtstar = 0.1332, omstar = 0.0455, rhofrac=0.6000, omega=0.4, xzrad=.7, tstar=7.2, K=1.88, mt=0.06`
my fav:
`omtstar = 0.0948, omstar = 0.0140, rhofrac=0.4500, omega=0.38, xzrad=0.7, tstar=9, K=2.07, mt=0.054`
also not bad:
`rhofrac=0.55, omega=0.3, xzrad=0.25` -- used in the preprint



#### no
Examples in `ricstab_main.py`
---

### Oscillator model
```
osc = OsciModel(b2=.5)
xzrad, theta = 1., np.pi/3
inia = xzrad*np.array([[np.cos(theta), np.sin(theta)]]).T
gamma = 1e-2
B = np.array([1., 0.]).reshape((2, 1))
C = np.array([1., 1.]).reshape((1, 2))
```
gave a nice picture ... eeeh only with the wrong feedback. This example obviously does not work as an example for *the static (initial) feedback will not stabilize the stabilize the system --> we need dynamic/nonlinear feedback*

Unstable behavior only for high `gamma=1000` or so.

Need check other exas - maybe higher dimensional.

### 5D example from BanLT
Without feedback and with default inival
```
    self.definival = np.array([.4, -.2, .1, -.1, .5]).reshape((5, 1))
```
blowup at about `t=2.3`. 

With initial feedback, `gamma=1e-5`, and
```
	self.B = np.array([[0., 0.], [0., 0.], [1., 0.], [0., 0.], [0., 1.]])
	self.C = self.B.T
```
stable...

And for almost all random initial feedbacks on the ball with `xzrad=.5`.
