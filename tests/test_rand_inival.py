import numpy as np
import matplotlib.pyplot as plt

N = 1000


def get_rand_inia(sdim, rad=1.):
    inia = np.random.randn(sdim, 1)
    return rad*1./np.linalg.norm(inia, ord=2)*inia

inial = []
for n in range(N):
    iniar = get_rand_inia(2, rad=1.)
    inial.append(iniar.flatten())
iniarray = np.array(inial)
xs, ys = iniarray[:, 0], iniarray[:, 1]
plt.plot(xs, ys, 'o')
plt.show(block=False)
