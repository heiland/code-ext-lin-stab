import numpy as np
import scipy.sparse as sps
import numpy.linalg as npla
import scipy.linalg as spla

N, p, q = 100, 2, 3
diffafac = 1e-4

A = sps.rand(N, N, density=0.2).todense()
B = sps.rand(N, p, density=0.3).todense()
C = sps.rand(q, N, density=0.3).todense()

gamma = 1e-3

brinvbt = 1./gamma*B.dot(B.T)
Q = C.T.dot(C)
R = gamma*np.eye(B.shape[1])

X = spla.solve_continuous_are(A, B, Q, R)
Z = A - brinvbt.dot(X)

deltaA = diffafac*sps.rand(N, N, density=0.4).todense()
epsP = spla.solve_sylvester(A, -Z, -deltaA)
opepspinv = npla.inv(epsP+np.eye(epsP.shape[0]))

print 'in ipython run \n%timeit curX = spla.solve_continuous_are(A, B, Q, R)\n%timeit epsP = spla.solve_sylvester(A, -Z, -deltaA)\n%timeit opepspinv = npla.inv(epsP+np.eye(epsP.shape[0]))'
