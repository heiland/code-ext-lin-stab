import numpy as np
import numpy.linalg as npla
import scipy.linalg as spla
# import matplotlib.pyplot as plt
# from scipy.integrate import odeint
# import sdre_models as sdrm
# import small_mat_utils as smu

import logging

logger = logging.getLogger('basic.ssu')


def get_fb_upd(amat, t, fbtype=None, wnrm=2,
               B=None, R=None, Q=None, maxeps=None,
               baseA=None, baseZ=None, baseP=None, maxfac=None, **kwargs):
    if fbtype == 'sylvupdfb' or fbtype == 'singsylvupd':
        deltaA = amat - baseA
        epsP = spla.solve_sylvester(amat, -baseZ, -deltaA)
        eps = npla.norm(epsP, ord=wnrm)
        if maxeps is not None:
            if eps < maxeps:
                opepsPinv = npla.inv(epsP+np.eye(epsP.shape[0]))
                return baseP.dot(opepsPinv), True
        elif maxfac is not None:
            if (1+eps)/(1-eps) < maxfac and eps < 1:
                opepsPinv = npla.inv(epsP+np.eye(epsP.shape[0]))
                return baseP.dot(opepsPinv), True

    # otherwise: (SDRE feedback or `eps` too large already)
    curX = spla.solve_continuous_are(amat, B, Q, R)
    if fbtype == 'sylvupdfb' or fbtype == 'singsylvupd':
        logger.debug('in `get_fb_dict`: t={0}: eps={1} too large, switch!'.
                     format(t, eps))
    else:
        logger.debug('t={0}: computed the SDRE feedback')
    return curX, False


def get_rand_inia(sdim, rad=1.):
    """ a point from a uniform distribution of points on the n-sphere

    cf. https://en.wikipedia.org/wiki/N-sphere#Generating_random_points
    """

    inia = np.random.randn(sdim, 1)
    return rad*1./np.linalg.norm(inia, ord=2)*inia
