import numpy as np
import numpy.linalg as npla
import matplotlib.pyplot as plt
from scipy.integrate import odeint

import dolfin
from dolfin import assemble, nabla_grad, dx, inner

import dolfin_navier_scipy.dolfin_to_sparrays as dts


def plotmat(sol, t0=0.0, tE=1.0, x0=0.0, xE=1.0, fignum=None, tikzfile=None):
    if fignum is None:
        fignum = 111
    plt.figure(fignum)
    plt.imshow(sol, extent=[x0, xE, tE, t0])
    plt.xlabel('x')
    plt.ylabel('t')
    # if tikzfile is not None:
    #     save(tikzfile + '.tikz')

    plt.colorbar(orientation='horizontal')  # , shrink=.75)
    plt.show(block=False)


def expandvfunc(vvec, V=None, ininds=None):
    """
    Notes
    ---
    by now only zero boundary values
    """
    v0 = dolfin.Function(V)
    auxvec = np.zeros((V.dim(), 1))
    auxvec[ininds] = vvec.reshape((ininds.size, 1))
    v0.vector().set_local(auxvec)
    v0.rename('v', 'velocity')
    return v0


def schloegl_spacedisc(N=10, a=0.0, b=1.0, nonlpara=15.,
                       retgetsdc=False, retfemdict=False):

    mesh = dolfin.IntervalMesh(N, a, b)
    V = dolfin.FunctionSpace(mesh, 'CG', 1)

    u = dolfin.TrialFunction(V)
    v = dolfin.TestFunction(V)

    # assembling the Neumann control operator
    class RightNeumannBoundary(dolfin.SubDomain):
        def inside(self, x, on_boundary):
            return (on_boundary and dolfin.near(x[0], b))

    rnb = RightNeumannBoundary()
    bparts = dolfin.FacetFunction('size_t', mesh)
    bparts.set_all(0)
    rnb.mark(bparts, 1)
    ds = dolfin.Measure('ds')[bparts]

    contshfun = dolfin.Constant('1')
    bneumann = assemble(v*contshfun*ds(1))

    # boundaries and conditions
    ugamma = dolfin.Expression('0')

    def _diriboundary(x, on_boundary):
        return dolfin.near(x[0], a)

    diribc = dolfin.DirichletBC(V, ugamma, _diriboundary)

    mass = assemble(v*u*dx)
    stif = assemble(inner(nabla_grad(v), nabla_grad(u))*dx)

    M = dts.mat_dolfin2sparse(mass)
    A = dts.mat_dolfin2sparse(stif)

    M, _, bcdict = dts.condense_velmatsbybcs(M, [diribc], return_bcinfo=True)
    ininds = bcdict['ininds']
    A, rhsa = dts.condense_velmatsbybcs(A, [diribc])
    bneumann = bneumann.array()[ininds]
    B = bneumann.reshape((bneumann.size, 1))

    def schloegl_nonl(vvec, t=None):
        v0 = expandvfunc(vvec, V=V, ininds=ininds)
        bnl = nonlpara*assemble(v*((v0-v0*v0*v0))*dx)
        return bnl.array()[ininds]

    def sdcpart(vvec, t=None):
        """ assuming zero dirichlet """

        # do it the right way
        # v0 = expandvfunc(vvec, V=V, ininds=ininds)
        # sdcpart = nonlpara*assemble(v*(u-v0*v0*u)*dx)

        # do it the efficient way
        onemvvcsqr = 1 - vvec*vvec  # that will only give an approximation
        onemv0sqr = expandvfunc(onemvvcsqr, V=V, ininds=ininds)
        sdcpart = nonlpara*assemble(v*(onemv0sqr*u)*dx)

        sdcpart = dts.mat_dolfin2sparse(sdcpart)
        sdcpart, rhsa = dts.condense_velmatsbybcs(sdcpart, [diribc])
        return sdcpart

    if retfemdict:
        return M, A, B, rhsa, schloegl_nonl, dict(V=V, diribc=diribc,
                                                  ininds=ininds)
    elif retgetsdc:
        return M, A, B, rhsa, sdcpart, schloegl_nonl

    else:
        return M, A, B, rhsa, schloegl_nonl


if __name__ == '__main__':
    xmin, xmax, N = 0., 2., 40
    M, A, B, rhsa, getschloeglsdc, schloegl_nonl =\
        schloegl_spacedisc(N=N, a=xmin, b=xmax, retgetsdc=True)
    testvec = np.ones((N, 1))
    t0, tE, Nts = 0., 1.0, 100  # Nts is only where to save the sols
    tmesh = np.linspace(t0, tE, Nts+1)
    xmesh = np.linspace(xmin, xmax, N+1)
    inival = 0.2*np.sin(xmesh*np.pi)[:-1]

    mdns, adns = np.array(M.todense()), np.array(A.todense())
    minv = npla.inv(mdns)
    minva = np.dot(minv, adns)

    def schloeglfunc(avec, t):
        axx = getschloeglsdc(avec)
        minvax = minv*axx
        return -minva.dot(avec) + minvax.dot(avec)

    sol, infodict = odeint(schloeglfunc, inival.flatten(), tmesh,
                           atol=1e-7, rtol=1e-7, full_output=True)
    sol = np.c_[sol, np.zeros((Nts+1, 1))]
    plotmat(sol[:, :])
