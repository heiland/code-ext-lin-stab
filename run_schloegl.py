import numpy as np

from ricstab_main import testit

import logging
import logging.config
import yaml
logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger('basic')
logger.setLevel('ERROR')  # default is DEBUG

quicktest = True
quicktest = False

fulltest = False
fulltest = True

Nlist = [20, 40, 60, 80, 100]
ntests = 5

surfplots = False
uncodatfile = 'schloegl-unco.dat'
sylvdatfile = 'schloegl-sylvupd.dat'

# model, modelpars, inivtype = 'banlt5d', {}, 2
model = 'schlgl1d'
inivalpars = dict(fbtype='defini')
nonlpara, xmax = 5., 2.

t0, tE, Nts = 0., 3.0, 50  # Nts is only where to save the sols
tmesh = np.linspace(t0, tE, Nts)

atol, rtol = 1e-6, 1e-6  # 1.49e-8, 1.49e-8  # the tols for the odeint
femnormcor = True  # mimicking the M norm for the Schloegl model

gamma = 1e-1

storeonlyoutputs = False
fbstats = False  # whether to do the stats -- disable for timings
plotplease = False  # whether to plot the trajectories

testparas = dict(model=model,
                 inival=None, inivalpars=inivalpars,
                 gamma=gamma, tmesh=tmesh, normtype=2,
                 storeonlyoutputs=storeonlyoutputs, fbstats=fbstats,
                 plotplease=plotplease)

if quicktest:
    N = 20
    modelpars = dict(N=N, nonlpara=nonlpara, xmax=xmax)
    print 'N={0}'.format(N)
    testlist = [('sdrefb', 0),  ('sylvupdfb', .1),
                ('sylvupdfb', .5), ('sylvupdfb', .9)]

    if femnormcor:
        corrfac = 1. / ((modelpars['xmax'] - 0.0) / (modelpars['N'])**2)
        catol, crtol = corrfac*atol, corrfac*rtol
        logger.info('N={0}: corrected the tols to (atol, rtol) = ({1},{2})'.
                    format(modelpars['N'], catol, crtol))

    testit(testlist=testlist, modelpars=modelpars,
           atol=catol, rtol=crtol, **testparas)

if fulltest:
    testlist1 = [('sylvupdfb', .5)]*ntests
    testlist2 = [('sylvupdfb', .9)]*ntests
    testlist3 = [('sdrefb', .0)]*ntests
    testlist = []
    for k in range(len(testlist1)):  # zipping the tests
        testlist.extend([testlist1[k], testlist2[k], testlist3[k]])

    for N in Nlist:
        print 'N={0}'.format(N)

        modelpars = dict(N=N, nonlpara=nonlpara, xmax=xmax)

        if femnormcor:
            # the actual norm |y| needs multiplication with mass matrix
            # we mimick this by h*|y| < tol <=> |y| < tol / h
            corrfac = 1. / ((modelpars['xmax'] - 0.0) / (modelpars['N']))
            catol, crtol = corrfac*atol, corrfac*rtol
            logger.info('N={0}: corrected tols to (catol, crtol) = ({1},{2})'.
                        format(modelpars['N'], atol, rtol))

        testit(testlist=testlist, modelpars=modelpars,
               atol=catol, rtol=crtol, **testparas)

if surfplots:
    modelpars = dict(N=20, nonlpara=nonlpara, xmax=xmax)
    testlist = [('nofb', 0)]
    cmd, sol = testit(testlist=testlist, modelpars=modelpars,
                      atol=atol, rtol=rtol, retmodsol=True, **testparas)
    cmd.solsurfplot(sol, fignum=232, tmesh=tmesh, datfile=uncodatfile)

    modelpars = dict(N=20, nonlpara=nonlpara, xmax=xmax)

    testlist = [('sylvupdfb', 0.9)]
    cmd, sol = testit(testlist=testlist, modelpars=modelpars,
                      atol=atol, rtol=rtol, retmodsol=True, **testparas)
    cmd.solsurfplot(sol, fignum=343, tmesh=tmesh, datfile=sylvdatfile)
