import numpy as np
import scipy.linalg as spla
# import matplotlib.pyplot as plt
from scipy.integrate import odeint

import dolfin_navier_scipy.data_output_utils as dou
import matlibplots.conv_plot_utils as cpu

import sdre_models as sdrm
import sdc_stab_utils as ssu
import small_mat_utils as smu


import logging
import logging.config
import yaml
logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger('basic')
logger.setLevel('ERROR')  # default is DEBUG

modeldict = {'banlt5d': sdrm.BanLT5DModel,
             'sdrosci': sdrm.OsciModel,
             'schlgl1d': sdrm.Schloegl1DNeumanContrl}
fbswitchdct = {0: 'nofb', 1: 'staticfb',
               2: 'sdrefb', 3: 'sylvupdfb', 4: 'singsylvupd'}
inivsdict = {0: 'randini', 1: 'defini', 2: 'badini'}


def ini_simu(model='banlt5d', modelpars={},
             gamma=None, epsi=None,
             normtype=2,
             inival=None, inivalpars={},
             fbtype=None):
    cmd = modeldict[model](**modelpars)

    B = cmd.B
    C = cmd.C

    brinvbt = 1./gamma*B.dot(B.T)
    Q = C.T.dot(C)
    R = gamma*np.eye(B.shape[1])

    if inival is None:
        inival = cmd.getinival(**inivalpars)

    fbdict = dict(B=B, R=R, Q=Q, brinvbt=brinvbt, fbtype=fbtype)
    if fbtype == 'staticfb':
        curA = cmd.sysmat(inival)
        curX = spla.solve_continuous_are(curA, B, Q, R)
        brinvbtP = np.dot(brinvbt, curX)
        fbdict.update(dict(brinvbtP=brinvbtP))
    if fbtype == 'sylvupdfb' or fbtype == 'singsylvupd':
        curA = cmd.sysmat(inival)
        curX = spla.solve_continuous_are(curA, B, Q, R)
        afb = curA - brinvbt.dot(curX)
        fbdict.update(dict(baseA=curA, switchts=[],
                           baseZ=afb, baseP=curX,
                           wnrm=normtype,
                           maxeps=epsi,
                           brinvbt=brinvbt))
        if fbtype == 'singsylvupd':
            fbdict.update(dict(numupds=0, maxupds=1, prevP=None))

    return cmd, inival, fbdict


def fbrhsfunc(avec, t, fbdict=None, cmd=None):
    fbtype = fbdict['fbtype']
    axx = cmd.ffunc(avec)
    if fbtype == 'nofb':
        return axx
    if fbtype == 'staticfb':
        return axx - fbdict['brinvbtP'].dot(avec)
    if fbtype == 'sdrefb' or fbtype == 'sylvupdfb' or fbtype == 'singsylvupd':
        amat = cmd.sysmat(avec)
        if fbtype == 'singsylvupd' and fbdict['numupds'] >= fbdict['maxupds']:
            curP = fbdict['prevP']
        else:
            curP, updsuccess = ssu.get_fb_upd(amat, t, **fbdict)
            if not updsuccess and (fbtype == 'sylvupdfb' or
                                   fbtype == 'singsylvupd'):
                fbdict['baseA'] = amat
                fbdict['baseZ'] = amat - fbdict['brinvbt'].dot(curP)
                fbdict['baseP'] = curP
                if fbtype == 'singsylvupd':
                    fbdict['numupds'] += 1
                    logger.info('t={0}: newnumupds = {1}'.
                                format(t, fbdict['numupds']))
                if fbtype == 'sylvupdfb' or fbtype == 'singsylvupd':
                    fbdict['switchts'].append(t)
            if updsuccess and fbtype == 'singsylvupd':
                fbdict['prevP'] = curP
        if 'recordfbs' in fbdict:
            curbrinvbtP = np.dot(fbdict['brinvbt'], curP)
            fbdict['recordfbs'].update({t: cmd.sysmat(avec)-curbrinvbtP})
        return axx - np.dot(fbdict['brinvbt'], np.dot(curP, avec))


def get_fbstats(clmsdict, wnrm=2):
    ttmsh = np.array(clmsdict.keys())
    ttmsh.sort()
    eveccondl, omegsl = [], []
    for ct in ttmsh:
        curclm = clmsdict[ct]
        eigvlvcs, ccnd = smu.get_evecs(curclm, get_evec_cond=True,
                                       ret_evals=True, wnrm=wnrm)
        eveccondl.append(ccnd)
        ceigvls = eigvlvcs[0]
        logger.debug('t={0}, eigvals={1}'.format(ct, ceigvls))
        omegsl.append(np.max(np.real(ceigvls)))

    return eveccondl, omegsl, ttmsh.tolist()


def testit(testlist=None, model=None, modelpars={},
           inival=None, inivalpars=None,
           gamma=None, atol=None, rtol=None,
           tmesh=None, normtype=2,
           storeonlyoutputs=False, fbstats=False, plotplease=False,
           retmodsol=False):

    infolist = ''
    ttmeshl, omsl, legsl = [], [], []
    soltmshl, sollist = [], []
    for testcase in testlist:
        # tmesh = np.linspace(t0, tE, Nts+1)
        fbtype, epsi = testcase
        if fbtype == 'nofb' and model == 'banlt5d':
            tmesh = np.linspace(t0, tEnofb, Nts+1)
        cmd, inival, fbdict = \
            ini_simu(model=model, modelpars=modelpars,
                     inival=inival, inivalpars=inivalpars,
                     gamma=gamma, normtype=normtype,
                     epsi=epsi,
                     fbtype=fbtype)
        logger.info('inival={0}'.format(inival.tolist()))
        if fbstats:
            fbdict.update(dict(recordfbs={}))  # record the closed loop matrixs

        timerinfo = dict(elt=None)
        with dou.Timer(name='closed loop simu', logger=logger,
                       timerinfo=timerinfo):
            sol, infodict = odeint(fbrhsfunc, inival.flatten(), tmesh,
                                   (fbdict, cmd, ),
                                   atol=atol, rtol=rtol, full_output=True)
            if storeonlyoutputs:
                sol = np.dot(sol, cmd.C.T)

        sollist.extend(sol.T.tolist())
        soltmshl.extend([tmesh]*sol.shape[1])
        nsdrswts = len(fbdict['switchts']) if 'switchts' in fbdict else 'dna'
        logger.info('feedback {0}, func evals: {1}, num of sdre switchs {2}'.
                    format(fbtype, infodict['nfe'][-1], nsdrswts))

        # the base info for c'n'p to latex
        infostr = fbtype + ' & ${0}$'.format(epsi) +\
            ' & ${0}$ & ${1}$'.format(infodict['nfe'][-1], nsdrswts) +\
            ' & ${0:.3f}s$\\\\\n'.format(timerinfo['elt'])

        infolist = infolist + infostr
        if fbstats:
            evccnd, omegsl, ttmsh = get_fbstats(fbdict['recordfbs'],
                                                wnrm=normtype)
            ttmeshl.append(ttmsh)
            omsl.append(omegsl)
            legsl.append('$\\epsilon = {0}$'.format(epsi))

    print infolist

    if plotplease:
        cpu.paraplot(None, sollist, abscissal=soltmshl, xlabel='t',
                     ylabel='$\\xi$',  # leglist=legsl,
                     title=None, fignum=112,
                     tikzfile='soltrajcs.tikz')

    if fbstats:
        cpu.paraplot(None, omsl, abscissal=ttmeshl, xlabel='t',
                     ylabel='$\\omega$', leglist=legsl,
                     title=None, fignum=111,
                     tikzfile='omegs.tikz')

    if retmodsol:
        return cmd, sol


if __name__ == '__main__':
    # model, modelpars, inivtype = 'banlt5d', {}, 2
    model, modelpars, inivtype = 'schlgl1d', dict(N=20, nonlpara=5, xmax=2.), 1
    t0, tE, Nts = 0., 3.0, 50  # Nts is only where to save the sols
    tmesh = np.linspace(t0, tE, Nts)

    atol, rtol = 1.49e-8, 1.49e-8
    inivalpars = dict(fbtype='defini')

    tEnofb = .1162  # end point before the blowup (depends on the prob!!)
    tEstatfb = .18  # before blowup with static feedback
    # later `sp.integrate.odeint` is used that has a step size control
    gamma = 1e-3
    xzrad = 2.
    normtype = 2  # which norm to use
    femnormcor = True  # mimicking the M norm for the Schloegl model
    storeonlyoutputs = False
    # storeonlyoutputs = True
    fbstats = False  # whether to do the stats -- disable for timings
    plotplease = True  # whether to plot the trajectories
    plotplease = False  # whether to plot the trajectories

    if model == 'schlgl1d' and femnormcor:
        corrfac = 1. / ((modelpars['xmax'] - 0.0) / (modelpars['N'])**2)
        atol, rtol = corrfac*atol, corrfac*rtol
        logger.info('N={0}: corrected the tols to (atol, rtol) = ({1},{2})'.
                    format(modelpars['N'], atol, rtol))

    # ## epsi is taken unless it is >1 or `None`
    epsi = .5  # !<1 - max norm of eps

    fbtype = fbswitchdct[3]  # {0:'nofb', 1:'static', 2:'sdrefb', 3: 'updfb'}
    inivtype = inivsdict[inivtype]  # {0: 'randini', 1: 'defini', 2: 'badini'}
    testlist = [(fbtype, epsi)]

    # ### sdre vs. sylvupd
    testlist = [('sdrefb', 0),  # ('sylvupdfb', .1),
                ('sylvupdfb', .5), ('sylvupdfb', .9)]

    # ### sdre vs. 1-sylvupd
    # testlist = [('sdrefb', 0), ('singsylvupd', .6), ('singsylvupd', .9)]

    # ### for testing the run times
    # testlist = [('singsylvupd', .9)]*10
    # testlist = [('singsylvupd', .6)]*10
    # testlist1 = [('sylvupdfb', .5)]*10
    # testlist2 = [('sylvupdfb', .9)]*10
    # testlist3 = [('sdrefb', .0)]*10
    # testlist = []
    # for k in range(len(testlist1)):
    #     testlist.extend([testlist1[k], testlist2[k], testlist3[k]])

    testit(testlist=testlist, model=model, modelpars=modelpars,
           inival=None, inivalpars=inivalpars,
           gamma=gamma, atol=atol, rtol=rtol,
           tmesh=tmesh, normtype=2,
           storeonlyoutputs=storeonlyoutputs, fbstats=fbstats,
           plotplease=plotplease)
