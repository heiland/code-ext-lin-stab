import numpy as np
import numpy.linalg as npla

# parameters
k1, k2 = 1., 1.
b1, b2 = 1., -.6
# tests: for b2 = .05 --> stable
# tests: for b2 = .06 --> unstable
inivfac = np.sqrt(2)
inia = inivfac*np.array([[1., 1.]]).T


def sysmat(avec):
    absao = avec[0]**2
    return np.array([[-b1, k1*(1.+absao)], [-k1*(1.+absao), -b2]])


def evals(xvec):
    camat = sysmat(xvec)
    deta = npla.det(camat)
    traa = camat.trace()
    imgpart = np.sqrt(.25*traa**2 - deta + 0j)
    lone = traa/2 + imgpart
    ltwo = traa/2 - imgpart
    return lone, ltwo


def evecs(xvec, get_cond=True):
    camat = sysmat(xvec)
    lone, ltwo = evals(xvec)
    evone = np.array([[lone-camat[1, 1], camat[1, 0]]]).T
    evtwo = np.array([[ltwo-camat[1, 1], camat[1, 0]]]).T
    if get_cond:
        evmat = np._r[evone, evtwo]
        cond = np.linalg.norm(evmat)*np.linalg.norm(np.linalg.inv(evmat))
        print 'K <= {0}'.format(cond)
        return evone, evtwo, cond
    return evone, evtwo


if __name__ == '__main__':
    amat = sysmat(inia)
    lone, ltwo = evals(inia)
    evone, evtwo = evecs(inia)
    print np.dot(amat, evone) - lone*evone
    evmat = np.hstack([evone, evtwo])
    evmatinv = npla.inv(evmat)
    print np.dot(evmatinv.dot(amat), evmat)

    print '|A*inia| ', np.linalg.norm(amat.dot(inia))
    print '|A| ', np.linalg.norm(amat, ord=2)
    print '|U| ', np.linalg.norm(evmatinv, ord=2)
    print '|Uinv| ', np.linalg.norm(evmat, ord=2)
