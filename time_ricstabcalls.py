import sdre_models as sdrm
import numpy as np
import numpy.linalg as npla
import scipy.linalg as spla


modeldict = {'banlt5d': sdrm.BanLT5DModel,
             'sdrosci': sdrm.OsciModel}

model = 'banlt5d'
t0, tE, Nts = 0., 3., 200  # Nts is only where to save the sols
dt = (tE-t0)/Nts

cmd = modeldict[model]()
inival = cmd.badinival
gamma = 1e-3

B = cmd.B
C = cmd.C

brinvbt = 1./gamma*B.dot(B.T)
Q = C.T.dot(C)
R = gamma*np.eye(B.shape[1])

nextx = inival + dt*cmd.ffunc(inival)

amat = cmd.sysmat(nextx)
baseA = cmd.sysmat(inival)


curX = spla.solve_continuous_are(baseA, B, Q, R)
baseZ = baseA - brinvbt.dot(curX)

deltaA = amat - baseA
epsP = spla.solve_sylvester(amat, -baseZ, -deltaA)
opepspinv = npla.inv(epsP+np.eye(epsP.shape[0]))

print 'in ipython run \n%timeit curX = spla.solve_continuous_are(baseA, B, Q, R)\n%timeit epsP = spla.solve_sylvester(amat, -baseZ, -deltaA)\n%timeit opepspinv = npla.inv(epsP+np.eye(epsP.shape[0]))'
