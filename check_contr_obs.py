import numpy as np

A = np.array([[0., 1., 0., 0., 0.],
              [0., 0., 1., 0., 0.],
              [0., 0., 0., 0, 0.],
              [0, 0., 0., 0, 1.],
              [0., 0., 0., 0., 0.]])

B = np.array([[0., 0.], [0., 0.], [1., 0.], [0., 0.], [0., 1.]])
C = np.array([[1., 0., 0., 0., 0.],
              [0., 0., 0., 0., 0],
              [0., 0., 0., 1., 0]])
# C = np.eye(5)
# C = B.T

ctrl, obsl = [B], [C]
akb = B
cak = C
for k in range(A.shape[0]-1):
    akb = np.dot(A, akb)
    cak = np.dot(cak, A)
    ctrl.append(akb)
    obsl.append(cak)

ctrlm = np.hstack(ctrl)
obsm = np.vstack(obsl)

print ctrlm
print obsm
