import numpy as np

from ricstab_main import testit

import logging
import logging.config
import yaml
logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger('basic')
logger.setLevel('ERROR')  # default is DEBUG

quicktest = True

fulltest = True
ntests = 10

# model, modelpars, inivtype = 'banlt5d', {}, 2
model = 'banlt5d'
inivalpars = dict(inivtype='badini')

t0, tE, Nts = 0., 3.0, 50  # Nts is only where to save the sols
tmesh = np.linspace(t0, tE, Nts)

atol, rtol = 1e-6, 1e-6  # 1.49e-8, 1.49e-8  # the tols for the odeint

gamma = 1e-3

fbstats = False  # whether to do the stats -- disable for timings
plotplease = False  # whether to plot the trajectories

testparas = dict(model=model,
                 inival=None, inivalpars=inivalpars,
                 gamma=gamma, tmesh=tmesh, normtype=2,
                 fbstats=fbstats,
                 plotplease=plotplease)

if quicktest:
    testlist = [('sdrefb', 0),  ('sylvupdfb', .1),
                ('sylvupdfb', .5), ('sylvupdfb', .9)]

    testit(testlist=testlist, atol=atol, rtol=rtol, **testparas)

if fulltest:
    testlist0 = [('sylvupdfb', .1)]*ntests
    testlist1 = [('sylvupdfb', .5)]*ntests
    testlist2 = [('sylvupdfb', .9)]*ntests
    testlist3 = [('sdrefb', .0)]*ntests
    testlist = []
    for k in range(len(testlist1)):  # zipping the tests
        testlist.extend([testlist0[k], testlist1[k],
                         testlist2[k], testlist3[k]])

    testit(testlist=testlist, atol=atol, rtol=rtol, **testparas)
